import React, { Component } from "react";
import { getData } from './utils/api';
import ListItems from "./components/ListItems";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      error: null,
    };
  }
  
  componentDidMount() {
    //Fetching using the getData api in Utils and saving the data to state
    getData('https://www.anapioficeandfire.com/api/houses')
      .then(data => this.setState({
        data
      }))
      .catch(error => this.setState({
        error
      }));
  }

  render() {
    var data = this.state.data ? this.state.data : [];
    const loading = <div className="container">
                      <h4>Loading...</h4>
                    </div>;
    
    return (
      <div className="App container" role='Main'>
      {this.state.data ? 
          <ListItems items={data}  /> : loading 
      }
      </div>
    );
  }
}

export default App;
