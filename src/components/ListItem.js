import React from "react";

const ListItem = (props) => {
    const items = props.items.map( item => {
        return(
            <li key={item.name} href={item.url}  className="card" >
                <div className="card-body">
                    <h3 className="card-title"> Name : {item.name} </h3>
                    Region : {item.region}
                    <p className="card-text">Coat of Arms : {item.coatOfArms}</p>
                    <a href={item.url} className="card-link">House Api</a>
                    <p><b>{item.words}</b> </p>
                </div>
            </li>
        )
    })
    return (
            <ul>
                {items}
            </ul>     
        );
};
      
export default ListItem;