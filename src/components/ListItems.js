import React , { Component } from "react";
import ListItem from "./ListItem";

class ListItems extends Component {
    render() {
        return(
            <section className="card--wrapper">
               <h1 className="text-center">Houses</h1>
                <ListItem 
                    items={this.props.items}
                /> 
            </section>

        )
    }
}

export default ListItems;