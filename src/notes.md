I have set up the folder structure with `Components`, Idealy i would change it to below. But this is simple enough.

My ideal folder structure 
```
Components
├── ListItems
|   |── ListItems.js 
|   |── ListItems.test.js 
│   └── ListItems.css
```

## Things to improve

Add more test for api calls, component rendering
Add more styling using sass
Add default props
Have onclick methods for listitems insted of an `<a>`