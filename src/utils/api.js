/**
 * Get Api
 */

export const getData = (url) => {
    return (
        fetch(url)     
            .then(result => result.json())
            .then(data => data)
    )
}

